package com.wycdrive.nyc.util;

import android.content.Context;
import android.util.Log;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClientOption;
import com.wycdrive.nyc.main.LocationApplication;

/***
 * 单点定位示例，用来展示基本的定位结果，配置在LocationService.java中
 * 默认配置也可以在LocationService中修改
 * 默认配置的内容自于开发者论坛中对开发者长期提出的疑问内容
 *
 * @author baidu
 *
 */
public class LocationUtil {

    public static double lon;
    public static double lat = -10000;
    public static String name;
    public static String description;

    private final Context context;
    private final BDAbstractLocationListener mListener;

    public LocationUtil(Context context) {
        this.context = context;
        this.mListener = new BDAbstractLocationListener() {
            @Override
            public void onReceiveLocation(BDLocation bdLocation) {
                lon = bdLocation.getLongitude();
                lat = bdLocation.getLatitude();
                name = "当前位置";
                description = bdLocation.getStreet();
                Log.d("ffffff", "onReceiveLocation: lat" + lat);
                Log.d("ffffff", "onReceiveLocation: lon" + lon);
            }
        };
        init();
    }

    public void stop() {
        LocationApplication.getLocationService().unregisterListener(mListener); //注销掉监听
        LocationApplication.getLocationService().stop(); //停止定位服务
    }

    public void start() {
        LocationApplication.getLocationService().start();
    }

    protected void init() {
        LocationApplication.getLocationService().registerListener(mListener);
        LocationClientOption option =LocationApplication.getLocationService().getDefaultLocationClientOption();
        option.setScanSpan(10000);
        //option.setLocationNotify(true);
        LocationApplication.getLocationService().setLocationOption(option);
    }

    public static double getDistance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double lat1 = (Math.PI / 180) * lat_a;
        double lat2 = (Math.PI / 180) * lat_b;

        double lon1 = (Math.PI / 180) * lng_a;
        double lon2 = (Math.PI / 180) * lng_b;

//      double Lat1r = (Math.PI/180)*(gp1.getLatitudeE6()/1E6);
//      double Lat2r = (Math.PI/180)*(gp2.getLatitudeE6()/1E6);
//      double Lon1r = (Math.PI/180)*(gp1.getLongitudeE6()/1E6);
//      double Lon2r = (Math.PI/180)*(gp2.getLongitudeE6()/1E6);

        //地球半径
        double R = 6371;

        //两点间距离 km，如果想要米的话，结果*1000就可以了
        double d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;

        return d*1000;

    }


}
