package com.wycdrive.nyc.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.wycdrive.nyc.model.vo.LocationNode;
import com.wycdrive.nyc.model.vo.NaviAddress;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FileAndSharedPreferenceUtil {


    private static String PREFERENCES_NAME = "com.wycdrive.nyc";

    public static void keepCurrentNaviStatus(Context context, int status) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("navistatus", status);
        editor.commit();
    }

    public static int getCurrentNaviStatus(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return pref.getInt("navistatus", 0);
    }


    public static void keepCookie(Context context, String cookies) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("cookies", cookies);
        editor.commit();
    }

    public static String readCookie(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return pref.getString("cookies", "");
    }

    public static void writeLocations(Context context, ArrayList<LocationNode> array) {
        ObjectOutputStream oos = null;
        try {
            File file = new File(context.getCacheDir() + File.separator + "locationnode");
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(array);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static ArrayList<LocationNode> readLocations(Context context) {
        File file = new File(context.getCacheDir() + File.separator + "locationnode");
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
            return (ArrayList<LocationNode>) ois.readObject();
        } catch (Exception e) {
            return null;
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeNaviAddress(Context context, NaviAddress address) {
        ObjectOutputStream oos = null;
        try {
            File file = new File(context.getCacheDir() + File.separator + "naviaddress");
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(address);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static NaviAddress readNaviAddress(Context context) {
        File file = new File(context.getCacheDir() + File.separator + "naviaddress");
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
            return (NaviAddress) ois.readObject();
        } catch (Exception e) {
            return null;
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }


}
