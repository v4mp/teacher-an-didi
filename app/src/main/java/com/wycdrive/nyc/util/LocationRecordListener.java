package com.wycdrive.nyc.util;

public interface LocationRecordListener {
    void onStartRecord();

    void onStopRecord();

    void onDestory();

}
