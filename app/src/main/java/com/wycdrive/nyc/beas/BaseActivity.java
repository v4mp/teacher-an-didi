package com.wycdrive.nyc.beas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.zhy.autolayout.AutoLayoutActivity;

/**
 * Created by anfeng on 16/5/9.
 * Activity的基类
 */
public abstract class BaseActivity extends AutoLayoutActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //加入销毁队列
        ActivityCollector.addActivity(this);

        super.onCreate(savedInstanceState);

        setContentView(getLayout());
        //初始化组件
        initView();
        //初始化数据
        initData();

    }

    protected abstract int getLayout();

    protected abstract void initView();

    protected abstract void initData();

    /**
     * 这个方法使组件实例化不需要转型
     * 使用方式:
     * TextView textView = bindView(R.id.tv);
     * 这样使用这个方法的时候是不需要强转的
     */
    protected <T extends View> T bindView(int id) {
        return (T) findViewById(id);
    }

    protected void goToAty(Context from, Class<? extends BaseActivity> to, Bundle bundle) {
        Intent intent = new Intent(from, to);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    protected void goToAty(Context from, Class<? extends BaseActivity> to) {
        Intent intent = new Intent(from, to);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        ActivityCollector.removeActivity(this);
        super.onDestroy();
    }

}
