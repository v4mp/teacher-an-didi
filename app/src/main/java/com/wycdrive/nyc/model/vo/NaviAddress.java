package com.wycdrive.nyc.model.vo;

import java.io.Serializable;

public class NaviAddress implements Serializable{

    /**
     * msg : 起始地目的地获取成功!
     * code : 0
     * data : {"sNode":{"longitude":1.1,"latitude":1.1,"name":"","description":""},"eNode":{"longitude":1.1,"latitude":1.1,"name":"","description":""}}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * sNode : {"longitude":1.1,"latitude":1.1,"name":"","description":""}
         * eNode : {"longitude":1.1,"latitude":1.1,"name":"","description":""}
         */

        private SNodeBean sNode;
        private ENodeBean eNode;

        public SNodeBean getSNode() {
            return sNode;
        }

        public void setSNode(SNodeBean sNode) {
            this.sNode = sNode;
        }

        public ENodeBean getENode() {
            return eNode;
        }

        public void setENode(ENodeBean eNode) {
            this.eNode = eNode;
        }

        public static class SNodeBean implements Serializable {
            /**
             * longitude : 1.1
             * latitude : 1.1
             * name :
             * description :
             */

            private double longitude;
            private double latitude;
            private String name;
            private String description;

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }

        public static class ENodeBean implements Serializable {
            /**
             * longitude : 1.1
             * latitude : 1.1
             * name :
             * description :
             */

            private double longitude;
            private double latitude;
            private String name;
            private String description;

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }
    }
}
