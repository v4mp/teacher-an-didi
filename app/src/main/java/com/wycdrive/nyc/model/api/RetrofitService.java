package com.wycdrive.nyc.model.api;


import com.wycdrive.nyc.model.vo.NaviAddress;
import com.wycdrive.nyc.model.vo.ResultData;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface RetrofitService {

    @FormUrlEncoded
    @POST
    Observable<ResultData> sendLocation(@Url String path, @Field("lon") double lon, @Field("lat") double lat, @Field("type") int type, @Field("oid") String oid);

    @FormUrlEncoded
    @POST("Home/Navigation/navigation")
    Observable<NaviAddress> getNaviAddress(@Field("type") int type, @Field("oid") String oid);

    @FormUrlEncoded
    @POST("Home/Navigation/jou")
    Observable<ResultData> sendDistance(@Field("jou") int distance, @Field("type") int type, @Field("oid") String oid);


    //====================================
    //获取验证码
    @FormUrlEncoded
    @POST("common/sendSmsCode")
    Observable<?> getVerifyCode(@Field("mobile") String mobile, @Field("type") String type);


}
