package com.wycdrive.nyc.model.vo;

import java.io.Serializable;

public class LocationNode implements Serializable {
    private double lat = -10000;
    private double lon = -10000;
    private long currentTime = -1;
    private int currentDistance = 0;

    public LocationNode(double lat, double lon, long currentTime) {
        this.lat = lat;
        this.lon = lon;
        this.currentTime = currentTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    @Override
    public String toString() {
        return "lat: " + lat + " lon: " + lon;
    }

    public double getCurrentDistance() {
        return currentDistance;
    }

    public void setCurrentDistance(int currentDistance) {
        this.currentDistance = currentDistance;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LocationNode) {
            return lat == ((LocationNode) obj).getLat() && lon == ((LocationNode) obj).getLon();
        }
        return false;
    }
}
