package com.wycdrive.nyc.model.api;

import android.text.TextUtils;

import com.wycdrive.nyc.GlobalConfig;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiManager {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private RetrofitService mDailyApi;
    private static ApiManager sApiManager;

    public static ApiManager getInstance() {
        if (sApiManager == null) {
            synchronized (ApiManager.class) {
                if (sApiManager == null) {
                    sApiManager = new ApiManager();
                }
            }
        }
        return sApiManager;
    }


    /**
     * 封装配置API
     */
    public RetrofitService getDataService() {
        OkHttpClient client = new OkHttpClient.Builder()
                //添加应用拦截器
                //.addInterceptor(new LoggerInterceptor("FFFFFF", true))
                //.addInterceptor(new ErrorInterceptor("FFFFFF"))
                //添加网络拦截器
                .addNetworkInterceptor(new LoggerInterceptor("FFFFFF", true))
                .build();
        if (mDailyApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GlobalConfig.baseUrl)
                    //将client与retrofit关联
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mDailyApi = retrofit.create(RetrofitService.class);
        }
        return mDailyApi;
    }

    /**
     * 封装配置API
     */
    public RetrofitService getDataService(final String cookie) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        // 设置 Cookie
                        if (!TextUtils.isEmpty(cookie)) {
                            return chain.proceed(chain.request().newBuilder().header("Cookie", cookie).header("Accept-Encoding", "").header("Accept", "application/json").build());
                        }
                        return chain.proceed(chain.request());
                    }
                })
                //添加应用拦截器
                //.addInterceptor(new LoggerInterceptor("FFFFFF", true))
                //.addInterceptor(new ErrorInterceptor("FFFFFF"))
                //添加网络拦截器
                .addNetworkInterceptor(new LoggerInterceptor("FFFFFF", true))
                .build();
        if (mDailyApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GlobalConfig.baseUrl)
                    //将client与retrofit关联
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mDailyApi = retrofit.create(RetrofitService.class);
        }
        return mDailyApi;
    }


    public static RequestBody createJsonBody(Map map) {
        RequestBody body = RequestBody.create(ApiManager.JSON, JsonUtils.toJsonParams(map));
        return body;
    }


}
