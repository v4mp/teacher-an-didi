package com.wycdrive.nyc.model.api;

public class ResultException extends RuntimeException {
    private int errorCode;
    private String codestr;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getCodestr() {
        return codestr;
    }

    public void setCodestr(String codestr) {
        this.codestr = codestr;
    }

    public ResultException(Throwable cause) {
        super(cause);
    }

    public ResultException(String message, int code) {
        super(message);
        errorCode = code;
    }


    public ResultException(String message, String codestr) {
        super(message);
        this.codestr = codestr;
    }

}