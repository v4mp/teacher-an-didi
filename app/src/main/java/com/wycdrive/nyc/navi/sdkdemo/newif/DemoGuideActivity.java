/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.wycdrive.nyc.navi.sdkdemo.newif;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.baidu.navisdk.adapter.BaiduNaviManagerFactory;
import com.baidu.navisdk.adapter.IBNRouteGuideManager;
import com.wycdrive.nyc.R;
import com.wycdrive.nyc.main.WebViewActivity;

/**
 * 诱导界面
 */
public class DemoGuideActivity extends Activity {

    private static final String TAG = "ffffff";

    // private BNRoutePlanNode mBNRoutePlanNode = null;

    private IBNRouteGuideManager mRouteGuideManager;
    //private LocationRecordUtil locationRecordUtil;

//    //导航经过点坐标
//    private ArrayList<LocationNode> naviNodes;
//
//   private int navi_status = 0;
//
//    private Disposable naviNodesDisposable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navi);
        mRouteGuideManager = BaiduNaviManagerFactory.getRouteGuideManager();
        View view = mRouteGuideManager.onCreate(this, mOnNavigationListener);
        if (view != null) {
            ((LinearLayout) findViewById(R.id.container)).addView(view);
        }
        findViewById(R.id.op).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemoGuideActivity.this, WebViewActivity.class).putExtra("URL", "http://www.bilibili.com"));
            }
        });
        //initNaviStatusAndNaviLocationNode();
        routeGuideEvent();
        //beginRecordLocation();

    }

//    private void initNaviStatusAndNaviLocationNode() {
//        Intent intent = getIntent();
//        if (intent != null) {
//            Bundle bundle = intent.getExtras();
//            if (bundle != null) {
//                navi_status = bundle.getInt(MainActivity.NAVI_STATUS);
//                naviNodes = FileAndSharedPreferenceUtil.readLocations(this);
//                if (naviNodes == null) {
//                    naviNodes = new ArrayList<>();
//                }
////                mBNRoutePlanNode = (BNRoutePlanNode)
////                        bundle.getSerializable(MainActivity.ROUTE_PLAN_NODE);
//            }
//        }
//    }

    // 导航过程事件监听
    private void routeGuideEvent() {
//        EventHandler.getInstance().getDialog(this);
//        EventHandler.getInstance().showDialog();

//        BaiduNaviManagerFactory.getRouteGuideManager().setRouteGuideEventListener(
//                new IBNRouteGuideManager.IRouteGuideEventListener() {
//                    @Override
//                    public void onCommonEventCall(int what, int arg1, int arg2, Bundle bundle) {
//                        EventHandler.getInstance().handleNaviEvent(what, arg1, arg2, bundle);
//                    }
//                }
//        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRouteGuideManager.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRouteGuideManager.onResume();
        // 自定义图层
        // showOverlay();
    }

//    private void showOverlay() {
//        BNOverlayItem item =
//                new BNOverlayItem(2563047.686035, 1.2695675172607E7, BNOverlayItem.CoordinateType.BD09_MC);
//        BNItemizedOverlay overlay = new BNItemizedOverlay(
//                DemoGuideActivity.this.getResources().getDrawable(R.drawable
//                        .navi_guide_turn));
//        overlay.addItem(item);
//        overlay.show();
//    }

    protected void onPause() {
        super.onPause();
        mRouteGuideManager.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mRouteGuideManager.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRouteGuideManager.onDestroy(false);
        // EventHandler.getInstance().disposeDialog();

    }

    @Override
    public void onBackPressed() {
        mRouteGuideManager.onBackPressed(false, true);
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mRouteGuideManager.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        if (!mRouteGuideManager.onKeyDown(keyCode, event)) {
            return super.onKeyDown(keyCode, event);
        }
        return true;

    }

//    private static final int MSG_RESET_NODE = 3;
//
//    private Handler hd = null;
//
//    private void createHandler() {
//        if (hd == null) {
//            hd = new Handler(getMainLooper()) {
//                public void handleMessage(android.os.Message msg) {
//                    if (msg.what == MSG_RESET_NODE) {
//                        mRouteGuideManager.resetEndNodeInNavi(mBNRoutePlanNode
//                        );
//                    }
//                }
//            };
//        }
//    }

    private IBNRouteGuideManager.OnNavigationListener mOnNavigationListener =
            new IBNRouteGuideManager.OnNavigationListener() {

                @Override
                public void onNaviGuideEnd() {
                    // 退出导航
                    finish();
                }

                @Override
                public void notifyOtherAction(int actionType, int arg1, int arg2, Object obj) {
                    if (actionType == PROFESSIONAL_NAVI_ACTION_ARRIVE_DEST) {
                        // 导航到达目的地 自动退出
                        Log.i(TAG, "notifyOtherAction actionType = " + actionType + ",导航到达目的地！");
                        mRouteGuideManager.forceQuitNaviWithoutDialog();

                    }
                }
            };


}
