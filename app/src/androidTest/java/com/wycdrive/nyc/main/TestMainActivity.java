package com.wycdrive.nyc.main;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
public class TestMainActivity {

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        Observable.just(1).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                assertEquals((int)integer, 1);
            }
        });
    }
}
